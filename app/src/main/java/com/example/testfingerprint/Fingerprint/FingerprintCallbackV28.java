package com.example.testfingerprint.Fingerprint;

import android.hardware.biometrics.BiometricPrompt;
import android.os.Build;

import androidx.annotation.RequiresApi;


@RequiresApi(api = Build.VERSION_CODES.P)
public class FingerprintCallbackV28 extends BiometricPrompt.AuthenticationCallback {

    private FingerprintCallback fingerprintCallback;
    public FingerprintCallbackV28(FingerprintCallback fingerprintCallback) {
        this.fingerprintCallback = fingerprintCallback;
    }


    //Fingerprint sesuai/sama dengan yang teregistrasi di device
    @Override
    public void onAuthenticationSucceeded(BiometricPrompt.AuthenticationResult result) {
        super.onAuthenticationSucceeded(result);
        fingerprintCallback.onAuthenticationSuccessful();
    }


    //Untuk non-fatal error, mengidentifikasi sebab error
    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        super.onAuthenticationHelp(helpCode, helpString);
        fingerprintCallback.onAuthenticationHelp(helpCode, helpString);
    }


    //Unrecoverable error dan proses autentikasi selesai tapi tidak berhasil/tidak sukses
    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
        super.onAuthenticationError(errorCode, errString);
        fingerprintCallback.onAuthenticationError(errorCode, errString);
    }


    //Fingerprint tidak sesuai/tidak sama dengan yang teregistrasi di device
    @Override
    public void onAuthenticationFailed() {
        super.onAuthenticationFailed();
        fingerprintCallback.onAuthenticationFailed();
    }
}
