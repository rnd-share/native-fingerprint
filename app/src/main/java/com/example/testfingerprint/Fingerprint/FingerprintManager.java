package com.example.testfingerprint.Fingerprint;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.hardware.biometrics.BiometricPrompt;
import android.os.Build;
import android.os.CancellationSignal;

import androidx.annotation.NonNull;

//Gabungan prompt dialog dan autentikasi fingerprint Android P ke atas dan versi Android di bawah Android P
public class FingerprintManager extends FingerprintManagerV23 {


    protected CancellationSignal mCancellationSignal = new CancellationSignal();

    protected FingerprintManager(final BiometricBuilder biometricBuilder) {
        this.context = biometricBuilder.context;
        this.title = biometricBuilder.title;
        this.subtitle = biometricBuilder.subtitle;
        this.description = biometricBuilder.description;
        this.negativeButtonText = biometricBuilder.negativeButtonText;
    }


    public void authenticate(@NonNull final FingerprintCallback fingerprintCallback) {

        if(title == null) {
            fingerprintCallback.onBiometricAuthenticationInternalError("Biometric Dialog title cannot be null");
            return;
        }


        if(subtitle == null) {
            fingerprintCallback.onBiometricAuthenticationInternalError("Biometric Dialog subtitle cannot be null");
            return;
        }


        if(description == null) {
            fingerprintCallback.onBiometricAuthenticationInternalError("Biometric Dialog description cannot be null");
            return;
        }

        if(negativeButtonText == null) {
            fingerprintCallback.onBiometricAuthenticationInternalError("Biometric Dialog negative button text cannot be null");
            return;
        }


        if(!FingerprintUtils.isSdkVersionSupported()) {
            fingerprintCallback.onSdkVersionNotSupported();
            return;
        }

        if(!FingerprintUtils.isPermissionGranted(context)) {
            fingerprintCallback.onBiometricAuthenticationPermissionNotGranted();
            return;
        }

        if(!FingerprintUtils.isHardwareSupported(context)) {
            fingerprintCallback.onBiometricAuthenticationNotSupported();
            return;
        }

        if(!FingerprintUtils.isFingerprintAvailable(context)) {
            fingerprintCallback.onBiometricAuthenticationNotAvailable();
            return;
        }

        displayBiometricDialog(fingerprintCallback);
    }

    public void cancelAuthentication(){
        if(FingerprintUtils.isBiometricPromptEnabled()) {
            if (!mCancellationSignal.isCanceled())
                mCancellationSignal.cancel();
        }else{
            if (!mCancellationSignalV23.isCanceled())
                mCancellationSignalV23.cancel();
        }
    }



    private void displayBiometricDialog(FingerprintCallback fingerprintCallback) {
        if(FingerprintUtils.isBiometricPromptEnabled()) {
            displayBiometricPrompt(fingerprintCallback);
        } else {
            displayBiometricPromptV23(fingerprintCallback);
        }
    }



    @TargetApi(Build.VERSION_CODES.P)
    private void displayBiometricPrompt(final FingerprintCallback fingerprintCallback) {
        new BiometricPrompt.Builder(context)
                .setTitle(title)
                .setSubtitle(subtitle)
                .setDescription(description)
                .setNegativeButton(negativeButtonText, context.getMainExecutor(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        fingerprintCallback.onAuthenticationCancelled();
                    }
                })
                .build()
                .authenticate(mCancellationSignal, context.getMainExecutor(),
                        new FingerprintCallbackV28(fingerprintCallback));
    }


    public static class BiometricBuilder {

        private String title;
        private String subtitle;
        private String description;
        private String negativeButtonText;

        private Context context;
        public BiometricBuilder(Context context) {
            this.context = context;
        }

        public BiometricBuilder setTitle(@NonNull final String title) {
            this.title = title;
            return this;
        }

        public BiometricBuilder setSubtitle(@NonNull final String subtitle) {
            this.subtitle = subtitle;
            return this;
        }

        public BiometricBuilder setDescription(@NonNull final String description) {
            this.description = description;
            return this;
        }


        public BiometricBuilder setNegativeButtonText(@NonNull final String negativeButtonText) {
            this.negativeButtonText = negativeButtonText;
            return this;
        }

        public FingerprintManager build() {
            return new FingerprintManager(this);
        }
    }
}
